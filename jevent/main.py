from config import Config
from flask import render_template
from app import create_app, create_celery, setdate
from app.events.models import Events, Members
from sqlalchemy import and_
from flask_mail import Mail, Message
from celery.utils.log import get_task_logger
from collections import defaultdict

# Calling create_app as an app
app = create_app(Config)


# Mail initialization
mail = Mail(app)

# Creating celery app
logger = get_task_logger(__name__)
celery = create_celery(app)


# Async Celery Task
@celery.task(name="async_task", max_retries=5)
def send_email(data):
    # Sending more than one email in one connection according to setting
    with mail.connect() as conn:
        # iterating email address
        for email in data['email']:
            msg = Message(data['subject'],
                          sender="admin@jflask.com",
                          recipients=[email])
            msg.body = data['content']
            mail.send(msg)
            # delay to prevent SMTP error
            time.sleep(5)
            logger.info("Async task complete for: {}".format(email))


# Periodic task, beat speed in setting/config
@celery.task(name="periodic_task")
def periodic_task():
    # Get time for UTC+8
    time_now = setdate()
    # Select timestamp where email sent status false
    qs_date = app.session.query(Events).filter(Events.sent == False)
    for date in qs_date:
        # Compare the timestamp with time now
        if date.timestamp <= time_now:
            try:
                # Joint EventModel and Member Model to get email address
                qs_mail = app.session.query(Events).filter(and_(Events.event_id == date.event_id),
                                                           (Events.members)).one()

                email_subject = qs_mail.email_subject
                email_content = qs_mail.email_content
                # Get email address
                data = defaultdict(list)
                for mail in qs_mail.members:
                    data['email'].append(mail.email)

                data['subject'] = email_subject
                data['content'] = email_content
                # Send async email
                print(data)
                send_email.apply_async([data], countdown=2)
                # Change sent status to true
                qs_mail.sent = True
                app.session.commit()
            except Exception as e:
                # When occurred must send email to administrator.
                # msg = Message("Error!",
                #               sender="system@jflask.com",
                #               recipients=["admin@jflask.com"])
                # msg.body = "Periodic task database connection Error."
                # mail.send(msg)
                return "Database connection failed, Please contact your administrator."

    logger.info("Periodic task run at {}".format(time_now))


# Custom error(404) handler
@app.errorhandler(404)
def page_not_found(e):
    # set the 404 status explicitly
    return render_template('404.html'), 404


# Remove session when tearing down the app context
@app.teardown_appcontext
def remove_session(*args, **kwargs):
    app.session.remove()


if __name__ == '__main__':
    app.run()
