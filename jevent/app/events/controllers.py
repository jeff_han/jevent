from flask import current_app, Blueprint, render_template, flash, redirect
from .models import Events, Members
from .forms import FormEvents, FormMembers

events_blueprint = Blueprint(
    'events_blueprint',
    __name__,
    template_folder='../templates/events',
)


@events_blueprint.route('/save_emails', methods=['GET', 'POST'])
def save_emails():
    form = FormEvents()
    try:
        # Select all from member to populate the choice
        form.member.choices = [
            (m.member_id, m.username)
            for m in current_app.session.query(Members).all()
        ]
    except Exception as e:
        return "Can't pull members data"

    # If form is valid
    if form.validate_on_submit():
        event_data = Events(
            email_subject=form.email_subject.data,
            email_content=form.email_content.data,
            timestamp=form.timestamp.data
        )

       # Save the relation between envent and member
        try:
            for member in form.member.data:
                qs = current_app.session.query(Members).filter(
                    Members.member_id == member).first()
                event_data.members.append(qs)

                current_app.session.add(event_data)
                current_app.session.commit()
            flash('Email saved with Subject: {}, Content: {}'.format(
                form.email_subject.data, form.email_content.data))
            return redirect('/')
        except Exception as e:
            print(e)
            current_app.session.rollback()
            return 'Data saving failed, Please try again'
    return render_template('save_emails.html', title='Save Emails', form=form)


@events_blueprint.route('/add-member', methods=['GET', 'POST'])
def add_member():
    form = FormMembers()
    # If form is valid
    if form.validate_on_submit():
        member_data = Members(
            email=form.email.data,
            username=form.username.data
        )
        # Saving data member
        try:
            current_app.session.add(member_data)
            current_app.session.commit()
            flash("Member {} successfuly saved.".format(form.username.data))
        except Exception as e:
            current_app.session.rollback()
            flash("Data saving failed, Please try again")
        return redirect("/")
    return render_template('add_member.html', title='Add Member', form=form)
