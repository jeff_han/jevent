from sqlalchemy import (
    ForeignKey, Column, Integer, Text, String, DateTime, Boolean
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Events(Base):
    '''Table events'''

    __tablename__ = 'events'

    event_id = Column(Integer, primary_key=True)
    email_subject = Column(String(100), nullable=False)
    email_content = Column(Text, nullable=False)
    timestamp = Column(DateTime, nullable=False)
    sent = Column(Boolean, default=False)

    # Relationships
    members = relationship(
        'Members', secondary='events_members_link', back_populates="events")

    # Representation
    def __repr__(self):
        return '<Events {}>'.format(self.event_id)


class Members(Base):
    '''Table members'''

    __tablename__ = 'members'

    member_id = Column(Integer, primary_key=True)
    email = Column(String(100), nullable=False)
    username = Column(String(100), nullable=False)

    # Relationships
    events = relationship(
        'Events', secondary='events_members_link', back_populates="members")

    # Representation
    def __repr__(self):
        return '<Members {}>'.format(self.member_id)


class EventsMembersLink(Base):
    '''Many to many relationship between events and members'''

    __tablename__ = 'events_members_link'
    event_id = Column(
        Integer, ForeignKey('events.event_id'), primary_key=True
    )
    member_id = Column(
        Integer, ForeignKey('members.member_id'), primary_key=True
    )
