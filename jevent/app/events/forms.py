from app import setdate
from flask_wtf import FlaskForm
from wtforms.widgets import TextArea
from wtforms import StringField, SubmitField, SelectMultipleField, DateTimeField
from wtforms.validators import DataRequired, ValidationError


def date_past_validation(form, field):
    """Check the date it should be greather than now"""

    if field.data < setdate():
        raise ValidationError("You can't schedule to the past")


class FormEvents(FlaskForm):
    """FlaskForm instance for Events"""

    email_subject = StringField('Subject', validators=[DataRequired()])
    email_content = StringField(
        'Content', widget=TextArea(),  validators=[DataRequired()])
    member = SelectMultipleField(
        'Member', coerce=int,  validators=[DataRequired()])
    timestamp = DateTimeField(
        'Events Date', validators=[DataRequired(), date_past_validation],
        default=setdate(), format='%Y-%m-%d %H:%M'
    )
    submit = SubmitField('Save Email')


class FormMembers(FlaskForm):
    """FlaskForm instance for Members"""

    email = StringField('Email', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired()])
    submit = SubmitField('Save Member')
