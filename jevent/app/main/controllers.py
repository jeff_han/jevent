from flask import Blueprint, current_app, render_template
from app.events.models import Events, Members

main_blueprint = Blueprint(
    'main_blueprint',
    __name__,
    template_folder='..templates',
)


@main_blueprint.route('/')
def index():
    try:
        event_obj = current_app.session.query(Events).filter(
            Events.members).order_by(Events.event_id.desc()).all()
    except Exception as e:
        print(e)
        return "Cannot pull data from the database, Please contact your administrator."
    context = {
        'obj': event_obj,
    }
    return render_template('home.html', title='Welcome to J-Event', context=context)
