import datetime
from flask import Flask, render_template, _app_ctx_stack
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import database_exists, create_database
from app.events.models import Base, Events, Members
# Enabling the CSRF
from flask_wtf.csrf import CSRFProtect
# Importing Celery
from celery import Celery


csrf = CSRFProtect()


def create_app(config_object):
    '''Creating the flask app'''
    from app.events.controllers import events_blueprint
    from app.main.controllers import main_blueprint

    app = Flask(__name__)
    app.config.from_object(config_object)

    # Initialize database engine
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    # Create database if not present
    if not database_exists(engine.url):
        create_database(engine.url)
    # Create SessionLocal factory
    SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    app.session = scoped_session(
        SessionLocal, scopefunc=_app_ctx_stack.__ident_func__)

    Base.metadata.create_all(bind=engine)
    app.register_blueprint(events_blueprint)
    app.register_blueprint(main_blueprint)

    csrf.init_app(app)
    return app


def create_celery(app):
    # Celery initialization
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __cal__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery


# Set date to UTC+8
def setdate():
    return (datetime.datetime.utcnow()+datetime.timedelta(hours=8))
