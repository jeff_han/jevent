import pytest

from flask import Flask
from sqlalchemy import SQLAlchemy


@pytest.fixture
def flask_app_mock():
    '''Database mocking connection test'''
    app_mock = Flask(__name__)
    db = SQLAlchemy(app_mock)
    db.init_app(app_mock)
    return app_mock
