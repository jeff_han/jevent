#!/bin/sh

gunicorn --config gunicorn_conf.py --bind 0.0.0.0:80 main:app & celery -A main.celery worker -l INFO & celery -A main.celery beat -l INFO

